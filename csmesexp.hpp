/*
 * csmesexp.hpp
 *
 *  Created on: 17 Mar 2017
 *      Author: s1249355
 */

#ifndef CSMES_EXP_HPP
#define CSMES_EXP_HPP

#include "csmes.h"

class CSMesExp : public CSMes {
public:
	QString exportCSMes() const;
};


#endif
