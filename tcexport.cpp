//============================================================================
// Name        : tcexport.cpp
// Author      : Boris Penev
// Version     :
// Copyright   : GPL
// Description : Test Cocoon Export in C++, Ansi-style
//============================================================================

/* #define OS_LINUX */
#include "libinstrumentationpdef.h"
#include "csmesexp.hpp"
#include "tcexport.hpp"
#include "Service.h"
#include <iostream>
#include <stdexcept>
#include <QList>
#include <unistd.h>
#include <QObject>
#include <QThread>
#include <QTextDocument>
#include <QFile>
#include <QBuffer>
#include <stdio.h>
#include <QDateTime>
#include <QTextStream>
#include <QProcess>
#include <QRegExp>
#include <QTime>

CSMesExp *openCSMesFile(const QString &filename) {
	CSMesExp *csmes_file_p;
	csmes_file_p = new CSMesExp();

	if (csmes_file_p->loadCSMes(filename)) {
		return csmes_file_p;
	} else {
		QString err = csmes_file_p->error();
		if (err == QString())
			throw std::runtime_error("Undefined error");
		throw std::runtime_error(err.toStdString());
	}
}

void openCSExeFile(CSMesExp *const csmes_file_p, const QString &filename) {
	CSMesIO::csexe_import_policy_t importPolicy =
			CSMesIO::CSEXE_POLICY_IGNORE_DUPLICATES;
	Executions::execution_status_t default_status =
			Executions::EXECUTION_STATUS_UNKNOWN;
	QString info, short_status;
	ExecutionNames new_measurements;
	QFile file(filename);
	QString err;

	if (!csmes_file_p->loadCSExe(file, filename, importPolicy, default_status,
			new_measurements, info, short_status, err, NULL, NULL)) {
		QString err = csmes_file_p->error();
		if (err == QString())
			throw std::runtime_error("Undefined error");
		throw std::runtime_error(err.toStdString());
	}

	//csmes_file_p->mergeExecutions(executionList(), new_measurements)
}

int main(int argc, char **argv) {
	if (argc < 2) {
		throw std::runtime_error("No filename given.");
	}
	QString filename(argv[1]);
	CSMesExp *csmes_file_p = openCSMesFile(filename);
	QString source = csmes_file_p->exportCSMes();
	std::cout << source.toStdString() << std::endl;
	for (int i = 2; i < argc; ++i) {
		QString filename(argv[i]);
		openCSExeFile(csmes_file_p, filename);
		QString source = csmes_file_p->exportCSMes();
		std::cout << source.toStdString() << std::endl;
	}
	delete csmes_file_p;
	csmes_file_p = NULL;
	return 0;
}
