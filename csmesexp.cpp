/*
 * csmesexp.cpp
 *
 *  Created on: 16 Mar 2017
 *      Author: s1249355
 */

#include "libinstrumentationpdef.h"
#include "csmesexp.hpp"
#include "Service.h"
#include <unistd.h>
#include <QObject>
#include <QThread>
#include <QTextDocument>
#include <QFile>
#include <stdio.h>
#include <QDateTime>
#include <QTextStream>
#include <QProcess>
#include <QRegExp>
#include <QTime>
#include "instrumentation.h"
#include "csexe_parser.h"
#include "csmesfile.h"
#include "executions.h"

QString CSMesExp::exportCSMes() const {
	QString sourcesContent = QString();
	const ModuleFiles &modules = Modules();
//	const SourceFiles &s = SourceFiles();
	for (ModuleFiles::const_iterator mi = modules.begin(), me = modules.end();
			mi != me; ++mi) {
		const ModuleFile &module = *mi;
		const SourceFiles &sources = Sources(static_cast<SourceFile>(module));
		for (SourceFiles::const_iterator si = sources.begin(), se =
				sources.end(); si != se; ++si) {
			const SourceFile &source = *si;
			QString src = QString();
			read_source(module, source, CSMESFile::ORIGINAL, src);
			sourcesContent += src;
		}
	}

	QString executionsContent = QString();
	// const ExecutionNames executionNames = selectedExecutions();
	const ExecutionNames executionNames = executionList();
	for (ExecutionNames::const_iterator ei = executionNames.begin(), ee =
			executionNames.end(); ei != ee; ++ei) {
		const ExecutionName execution = *ei;
		executionsContent += execution.toQString();
		executionsContent += "\n";
	}
	if (executionsContent.isEmpty()) {
		executionsContent = "Empty\n";
	} else {
		executionsContent += '\n';
	}

	QString executionsContent2 = QString();
	static const char *execStatusStrings[] = { "EXECUTION_STATUS_UNKNOWN",
			"EXECUTION_STATUS_FAILED", "EXECUTION_STATUS_PASSED",
			"EXECUTION_STATUS_TO_BE_CHECK_MANUALLY" };
	static const char *instTypeStrings[] = { "COMPUTATION", "LABEL",
			"CONDITION_FULL", "CONDITION_TRUE_ONLY", "CONDITION_FALSE_ONLY",
			"NOP", "TYPE_NULL" };
	for (ExecutionNames::const_iterator ei = executionNames.begin(), ee =
			executionNames.end(); ei != ee; ++ei) {
		const ExecutionName executionName = *ei;
		executionsContent2 += "execution name ";
		executionsContent2 += executionName.toQString();
		executionsContent2 += "\n";
		Executions::modules_executions_t execution = executions.getExecution(
				executionName);
		Executions::execution_status_t exec_status =
				execution.executionStatus();
		executionsContent2 += "master execution status ";
		executionsContent2 += execStatusStrings[exec_status];
		executionsContent2 += "\n";
		Executions::list_modules_executions_t executions = execution.executions;
		for (ModuleFiles::const_iterator mi = modules.begin(), me =
				modules.end(); mi != me; ++mi) {
			const ModuleFile module = *mi;
			const SourceFiles sources = Sources(
					static_cast<SourceFile>(module));
			for (SourceFiles::const_iterator si = sources.begin(), se =
					sources.end(); si != se; ++si) {
				const SourceFile source = *si;
				const CSMesInstrumentations::Instrumentations inst =
						instrumentations.modules[module].sources[source].instrumentations;
				executionsContent2 += source + '\n';
				Executions::executions_t exect = executions[module];
				CSMesInstrumentations::Instrumentations::const_iterator ii =
						inst.begin(), ie = inst.end();
				executionsContent2 += "instrumentation size: ";
				executionsContent2 += QString::number(inst.size()) + '\n';
				executionsContent2 += "executions size: ";
				executionsContent2 += QString::number(exect.size()) + '\n';
				for (Executions::executions_t::const_iterator execState =
						exect.begin(); execState != exect.end() && ii != ie;
						++execState, ++ii) {
					const Instrumentation instrumentation = *ii;
					executionsContent2 += "explanation:\n";
					executionsContent2 += instrumentation.explanationPlainText(
							source, 1, instrumentation.COVERAGE_CONDITION)
							+ '\n';
					executionsContent2 += "instrumentation type: ";
					executionsContent2 +=
							instTypeStrings[instrumentation.getType()];
					executionsContent2 += '\n';
					executionsContent2 += QString::number(
							instrumentation.getMinIndex()) + ' ';
					executionsContent2 += QString::number(
							instrumentation.getMaxIndex()) + ' ';
					executionsContent2 += QString::number(
							instrumentation.startLineOrg()) + ' ';
					executionsContent2 += "\n";
					// executionsContent2 += QString::number(instrumentation.executionCount()) + ' ';
					executionsContent2 += "instrumentation executions:\n";
					for (int i = instrumentation.getMinIndex();
							i <= instrumentation.getMaxIndex(); ++i) {
						executionsContent2 += QString::number(
								instrumentation.getExecution(i));
						executionsContent2 += "\n";
					}
					executionsContent2 += "state: ";
					executionsContent2 += QString::number(*execState);
					executionsContent2 += "\n";
				}
			}
		}
	}
	if (executionsContent2.isEmpty()) {
		executionsContent2 = "Empty\n";
	} else {
		executionsContent2 += '\n';
	}

	QList<int> commentedLines = QList<int>();
	for (ModuleFiles::const_iterator mi = modules.begin(), me = modules.end();
			mi != me; ++mi) {
		const ModuleFile &module = *mi;
		const SourceFiles &sources = Sources(static_cast<SourceFile>(module));
		for (SourceFiles::const_iterator si = sources.begin(), se =
				sources.end(); si != se; ++si) {
			const SourceFile &source = *si;
			commentedLines.append(commentedLinesPre(module, source));
		}
	}
	qSort(commentedLines);
	QString commentedLinesContent = QString();
	for (QList<int>::const_iterator cli = commentedLines.begin(), cle =
			commentedLines.end(); cli != cle; ++cli) {
		commentedLinesContent += QString::number(*cli);
	}
	if (commentedLinesContent.isEmpty()) {
		commentedLinesContent = "Empty\n";
	} else {
		commentedLinesContent += '\n';
	}

	QList<int> instrumentationLines = QList<int>();
	QString instrumentationLinesContent = QString();
	for (ModuleFiles::const_iterator mi = modules.begin(), me = modules.end();
			mi != me; ++mi) {
		const ModuleFile &module = *mi;
		const SourceFiles &sources = Sources(static_cast<SourceFile>(module));
		for (SourceFiles::const_iterator si = sources.begin(), se =
				sources.end(); si != se; ++si) {
			const SourceFile &source = *si;
			const CSMesInstrumentations::Instrumentations &inst =
					instrumentations.modules[module].sources[source].instrumentations;
			for (CSMesInstrumentations::Instrumentations::const_iterator ii =
					inst.begin(), ie = inst.end(); ii != ie; ++ii) {
				const Instrumentation instrumentation = *ii;
				for (int line = instrumentation.startLinePre();
						line <= instrumentation.endLinePre(); ++line) {
					if (!instrumentationLines.contains(line))
						instrumentationLines.append(line);
				}
			}
			qSort(instrumentationLines);
			instrumentationLinesContent += source + '\n';
			instrumentationLinesContent += "instrumentations at lines ";
			for (QList<int>::const_iterator cli = instrumentationLines.begin(),
					cle = instrumentationLines.end(); cli != cle; ++cli) {
				instrumentationLinesContent += QString::number(*cli) + ' ';
			}
			instrumentationLinesContent += '\n';
		}
	}
	if (instrumentationLinesContent.isEmpty()) {
		instrumentationLinesContent = "Empty\n";
	} else {
		instrumentationLinesContent += '\n';
	}

	QList<int> instrumentationExplanation = QList<int>();
	QString instrumentationExplanationContent = QString();
	for (ModuleFiles::const_iterator mi = modules.begin(), me = modules.end();
			mi != me; ++mi) {
		const ModuleFile &module = *mi;
		const SourceFiles &sources = Sources(static_cast<SourceFile>(module));
		for (SourceFiles::const_iterator si = sources.begin(), se =
				sources.end(); si != se; ++si) {
			const SourceFile &source = *si;
			const CSMesInstrumentations::Instrumentations &inst =
					instrumentations.modules[module].sources[source].instrumentations;
			instrumentationExplanationContent += source + '\n';
			for (CSMesInstrumentations::Instrumentations::const_iterator ii =
					inst.begin(), ie = inst.end(); ii != ie; ++ii) {
				const Instrumentation instrumentation = *ii;
				static const char *enumStrings[] = { "COMPUTATION", "LABEL",
						"CONDITION_FULL", "CONDITION_TRUE_ONLY",
						"CONDITION_FALSE_ONLY", "NOP", "TYPE_NULL" };
				instrumentationExplanationContent += QString::number(
						ii->startLinePre());
				instrumentationExplanationContent += QString(":")
						+ QString::number(ii->startColumnPre());
				instrumentationExplanationContent += QString(" - ")
						+ QString::number(ii->endLinePre());
				instrumentationExplanationContent += QString(":")
						+ QString::number(ii->endColumnPre());
				instrumentationExplanationContent += " ";
				instrumentationExplanationContent += enumStrings[ii->getType()];
				instrumentationExplanationContent += " ";
				instrumentationExplanationContent += QString::number(
						ii->executionCountMax());
				instrumentationExplanationContent += "\n";
//				if (ii->isConditionInstrumentation()) {
//					instrumentationExplanationContent +=
//							ii->explanationPlainText(source, 1,
//									instrumentation.COVERAGE_CONDITION) + '\n';
//				}
			}
			instrumentationExplanationContent += '\n';
		}
	}
	if (instrumentationExplanationContent.isEmpty()) {
		instrumentationExplanationContent = "Empty\n";
	} else {
		instrumentationExplanationContent += '\n';
	}

	return executionsContent2;
}
